/*
Este archivo se ha generado y no es el código fuente actual para esta
clase global gestionada.
Este archivo de solo lectura muestra los métodos, las variables, las propiedades
y los constructores globales de la clase.
Para permitir al código compilar, todos los métodos devuelven nulo.
*/
@RestResource(urlMapping='/WelcomeFinder/*')
global class WelcomeFinder {
    global WelcomeFinder() {

    }
    @HttpGet
    global static Map<String,String> getStartUrl() {
        return null;
    }
}
